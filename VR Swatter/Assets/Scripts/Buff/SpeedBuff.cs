using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBuff : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Kokonut")
        {
            Kokonut koko = other.gameObject.GetComponent<Kokonut>();
            koko.StartSpeedBuffTimer();
            Destroy(gameObject);
        }
    }
}
