﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet_buff : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Kokonut")
        {
            Kokonut koko = other.gameObject.GetComponent<Kokonut>();
            koko.StartMagnetBuffTimer();
            Destroy(gameObject);
        }
    }
}
