﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class KokonutIndicator : MonoBehaviour {

    private List<GameObject> _activePlayers;
    private new Camera camera;

    private Plane[] planes;
    private string KokonutId;

    private float minTextSize = 0.02f;
    private float maxTextSize = 0.1f;

    // Use this for initialization
    void Start () {
        camera = GetComponentInChildren<Camera>();
        KokonutId = GetComponent<Kokonut>().GetID();
    }
	
	// Update is called once per frame
	void Update () {
        _activePlayers = GameObject.FindGameObjectsWithTag("Kokonut").Where(player => player.activeInHierarchy).ToList();

        _activePlayers.ForEach(player =>
        {
            if (player != gameObject)
            {
                onScreen(player);
            }
        });
    }

    private void onScreen(GameObject obj)
    {
        planes = GeometryUtility.CalculateFrustumPlanes(camera);
        Collider objCollider = obj.GetComponent<Collider>();

        // If object we are looking for is in the camera view
        // Turn indicator on and set scale relative to distance (further => larger)
        if (GeometryUtility.TestPlanesAABB(planes, objCollider.bounds))
        {
            getIndicator(obj).SetActive(true);
            float dist = Vector3.Distance(camera.transform.position, getIndicator(obj).transform.position) * 0.001f;
            Vector3 v = camera.transform.position - getIndicator(obj).transform.position;
            v.x = v.z = 0.0f;
            getIndicator(obj).transform.LookAt(camera.transform.position - v);
            getIndicator(obj).transform.Rotate(20, 180, 0);

            Vector3 scale = new Vector3(Mathf.Clamp(dist, minTextSize, maxTextSize), Mathf.Clamp(dist, minTextSize, maxTextSize), 0);
            getIndicator(obj).transform.localScale = scale;
        }
        else
        {
            getIndicator(obj).SetActive(false);
        }
    }

    // Get the Indicator game object respective to the game object on the screen and to this game object.
    private GameObject getIndicator(GameObject obj)
    {
        string canvas = obj.GetComponent<Kokonut>().GetID() + "-" + KokonutId + "/";
        return obj.transform.Find(canvas + obj.GetComponent<Kokonut>().GetID() + "-" + KokonutId).gameObject;
    }
}
