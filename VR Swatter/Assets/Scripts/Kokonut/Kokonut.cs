using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class Kokonut : MonoBehaviour
{
    [Header("Helpers: show players controls in game")]
    public GameObject DiveHelper;
    public GameObject JumpHelper;

    [Tooltip("This is the max number of seconds a helper will show on the screen")]
    public float HelperDurationSeconds = 5f;
    public AudioSource KokokingLaugh;
    public AudioClip[] KokoKingLaughs;
    public AudioSource Laugh;
    public AudioSource disabled;
    private Motion motion;
    private int FeathersCollected;
    public int id;
    private int ControllerID;
    private bool Ready = false;

    private bool smacked = false;
    private bool smackedAnimTrigger = false;
    private float disabledTimer = 0f;
    private const float disabledDuration = 30f;
    public GameObject ButtonMash;
    private bool smackedAndDrop = false;
    private int numDrop = 1;

    // Buff Timer Indicator
    public Slider BuffTimerSlider;
    public float BuffTimerMaxValue = 10f;
    private GameObject MagnetImage;
    private GameObject SpeedImage;
    private Image BuffSliderBackground;
    private Image BuffSliderFill;
    private Color greyBackground;
    private Color greyFill;

    // Speed buff properties
    private bool speedBuffActivated = false;
    private float originalSpeed;
    private float originalMaxVelocity;
    private float speedBuffTimer = 0f;
    private const float speedBuffDuration = 10f;
    private float speedBuff = 2f;

    // Magnet Buff properties
    private bool buff_magnet = false;
    private float radius = 16.5f;
    private float magnetBuffTimer = 0f;
    private const float magnetBuffDuration = 8f;
    private float Attract_speed = 2f;
    private GameObject[] feathers;
    // Perma Magnet buff properties
    private float pRadius = 6.5f;
    private int _jumpCountAtDisplayJumpHelper;
    private int _diveCountAtDisplayDiveHelper;

    void Start()
    {
        DiveHelper.SetActive(false);
        JumpHelper.SetActive(false);

        FeathersCollected = 0;
        motion = GetComponent<Motion>();
        originalMaxVelocity = motion.maxVelocity;
        originalSpeed = motion.moveSpeed;

        MagnetImage = BuffTimerSlider.gameObject.transform.Find("Magnet").gameObject;
        SpeedImage = BuffTimerSlider.gameObject.transform.Find("Speed").gameObject;
        BuffSliderBackground = BuffTimerSlider.gameObject.transform.Find("Background").gameObject.GetComponent<Image>();
        BuffSliderFill = BuffTimerSlider.gameObject.transform.Find("Fill Area").gameObject.GetComponentInChildren<Image>();

        BuffTimerSlider.maxValue = BuffTimerMaxValue;
        greyBackground = BuffSliderBackground.color;
        greyFill = BuffSliderFill.color;

        foreach (KeyValuePair<int, string> entry in GameSettings.ControllerSet)
        {
            if (entry.Value == GetID())
            {
                ControllerID = entry.Key;
            }
        }
    }

    public void HitByKokonut()
    {
        smacked = true;
        smackedAndDrop = true;
    }

    private void Update()
    {
        if (!IsSmacked())
        {
            SpeedBuff();
        }

        if (buff_magnet)
        {
            if (!IsSmacked())
            {
                MagnetBuff();
            }
        }
        else
        {
            if (!IsSmacked())
            {
                PullFeather(pRadius);
            }
        }
        Disabled();
        UpdateHelperCanvas();
    }

    public string GetID()
    {
        return "P" + id.ToString();
    }

    public void SetID(int ID)
    {
        id = ID;
    }

    public string GetControllerID()
    {
        return "P" + ControllerID.ToString();
    }

    public void SetControllerID(int ID)
    {
        ControllerID = ID;
    }

    public void ReadyUp()
    {
        Ready = true;
    }

    public void NotReady()
    {
        Ready = false;
    }

    public bool IsReady()
    {
        return Ready;
    }

    public int GetNumFeathers()
    {
        return FeathersCollected;
    }

    public void CollectFeathers(int numFeathers)
    {
        FeathersCollected += numFeathers;
    }

    public void PlayLaugh()
    {
        Laugh.Play();
    }

    public void DropFeathers(int numFeathers)
    {
        FeathersCollected = Math.Max(0, FeathersCollected - numFeathers);
    }

    public void SmackAndDropFeathers()
    {
        if (IsSmacked() && smackedAndDrop)
        {
            DropFeathers(numDrop);
            smackedAndDrop = false;
        }
    }

    /*
     * Disabled by hit
     */
    public void GotSmacked()
    {
        smacked = true;
        smackedAndDrop = true;
        disabled.Play();
        transform.Find("Stars").gameObject.SetActive(true);
        KokokingLaugh.clip = KokoKingLaughs[UnityEngine.Random.Range(0, KokoKingLaughs.Length)];
        KokokingLaugh.Play();
    }

    public bool IsSmacked()
    {
        return smacked;
    }

    public void StartDisabledTimer(float timer = disabledDuration)
    {
        disabledTimer = timer;
        smackedAnimTrigger = true;
        transform.Find("KOKO").GetComponent<Animator>().SetTrigger("Disabled");
        transform.Find("KOKO").GetComponent<Animator>().SetTrigger("DisabledIdle");
    }

    private void Disabled()
    {
        if (disabledTimer > 0)
        {
            if (smacked)
            {
                motion.enabled = false;
                ButtonMash.SetActive(true);
                transform.Find(GetID() + " Camera/Canvas").GetComponent<Animator>().SetTrigger("ButtonMash");
            }

            if (Input.GetButtonDown(GetControllerID() + "_X"))
            {
                disabledTimer -= 5f;
            }

            disabledTimer -= Time.deltaTime;
        }
        else
        {
            if (smacked && smackedAnimTrigger)
            {
                transform.Find("KOKO").GetComponent<Animator>().SetTrigger("DisabledRecover");
                transform.Find("KOKO").GetComponent<Animator>().SetTrigger("Idle");
            }
            smacked = false;
            smackedAnimTrigger = false;
            motion.enabled = true;
            ButtonMash.SetActive(false);
            transform.Find("Stars").gameObject.SetActive(false);
            if (disabled.isPlaying)
            {
                disabled.Stop();
            }
        }
    }

    /* 
     * Speed Buff code
     */
    public void StartSpeedBuffTimer(float timer = speedBuffDuration)
    {
        speedBuffTimer = timer;
        BuffTimerSlider.maxValue = timer;
        BuffTimerSlider.value = speedBuffTimer;
        BuffSliderBackground.color = new Color32(0, 190, 255, 150);
        BuffSliderFill.color = new Color32(0, 150, 255, 255);
        SpeedImage.SetActive(true);
        transform.Find(GetID() + " Camera/Canvas").GetComponent<Animator>().SetTrigger("SpeedDisplay");
        transform.Find(GetID() + " Camera/Canvas").GetComponent<Animator>().SetTrigger("Default");
        transform.Find("KOKO").GetComponent<Animator>().SetTrigger("SpeedBoost");
        transform.Find("KOKO").GetComponent<Animator>().SetTrigger("Idle");
    }

    private void SpeedBuff()
    {
        if (speedBuffTimer > 0)
        {
            // If not activated, active it. One time action
            if (!speedBuffActivated)
            {
                SpeedChange(speedBuff);
                speedBuffActivated = true;
            }
            speedBuffTimer -= Time.deltaTime;
            BuffTimerSlider.value = speedBuffTimer;
        }
        else // Timer has run out
        {
            if (speedBuffActivated)
            {
                motion.moveSpeed = originalSpeed;
                motion.maxVelocity = originalMaxVelocity;
                speedBuffActivated = false;

                BuffTimerSlider.maxValue = BuffTimerMaxValue;
                BuffSliderBackground.color = greyBackground;
                BuffSliderFill.color = greyFill;
                SpeedImage.SetActive(false);
            }
        }
    }

    private void SpeedChange(float moveMultiplier)
    {
        motion.moveSpeed = motion.moveSpeed * moveMultiplier;
        motion.maxVelocity = 60f;
    }

    /*
     * Magnet Buff code
     */
    public void StartMagnetBuffTimer(float timer = magnetBuffDuration)
    {
        magnetBuffTimer = timer;
        BuffTimerSlider.maxValue = timer;
        BuffTimerSlider.value = magnetBuffTimer;
        BuffSliderBackground.color = new Color32(255, 100, 0, 150);
        BuffSliderFill.color = new Color32(255, 40, 0, 255);
        MagnetImage.SetActive(true);
        buff_magnet = true;
        transform.Find(GetID() + " Camera/Canvas").GetComponent<Animator>().SetTrigger("MagnetDisplay");
        transform.Find(GetID() + " Camera/Canvas").GetComponent<Animator>().SetTrigger("Default");
    }

    private void MagnetBuff()
    {
        if (magnetBuffTimer > 0)
        {
            PullFeather(radius);
            magnetBuffTimer -= Time.deltaTime;
            BuffTimerSlider.value = magnetBuffTimer;
        }
        else
        {
            BuffTimerSlider.maxValue = BuffTimerMaxValue;
            BuffSliderBackground.color = greyBackground;
            BuffSliderFill.color = greyFill;
            MagnetImage.SetActive(false);
            buff_magnet = false;
        }
    }

    /*
     * Pull the feather toward this kokonut 
     */
    private void PullFeather(float rds)
    {
        feathers = GameObject.FindGameObjectsWithTag("Feather");
        foreach (GameObject feather in feathers)
        {
            float distance = Vector3.Distance(transform.position, feather.transform.position);
            if (distance <= rds)
            {
                Vector3 magnetField = transform.position - feather.transform.position;
                float factor = 0.2f * (rds - magnetField.magnitude) / rds;
                feather.transform.position += (magnetField * factor * Attract_speed);
            }
        }
    }

    public static void Disable(GameObject kokonut, bool isDisabled = true)
    {
        var isEnabled = !isDisabled;
        kokonut.GetComponent<Motion>().enabled = isEnabled;
        kokonut.GetComponent<CollectFeather>().enabled = isEnabled;
        kokonut.GetComponent<DisableByHit>().enabled = isEnabled;
        kokonut.GetComponent<WindGustSpawn>().enabled = isEnabled;
        kokonut.GetComponent<Rigidbody>().velocity = Vector3.zero;
        kokonut.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }
    private void UpdateHelperCanvas()
    {
        if (motion.DiveCount > _diveCountAtDisplayDiveHelper)
        {
            DiveHelper.SetActive(false);
        }
        if (motion.JumpCount > _jumpCountAtDisplayJumpHelper)
        {
            JumpHelper.SetActive(false);
        }

    }
    public void DisplayDiveHelper()
    {
        _diveCountAtDisplayDiveHelper = motion.DiveCount;
        StartCoroutine(ShowHelper(DiveHelper));
    }

    public void DisplayJumpHelper()
    {
        _jumpCountAtDisplayJumpHelper = motion.JumpCount;
        StartCoroutine(ShowHelper(JumpHelper));
    }

    private IEnumerator ShowHelper(GameObject helper)
    {
        helper.SetActive(true);
        yield return new WaitForSeconds(5);
        helper.SetActive(false);
    }

}