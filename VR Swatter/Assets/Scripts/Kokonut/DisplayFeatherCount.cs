using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayFeatherCount : MonoBehaviour
{
    public Kokonut kokonut;
    public Text VRFeatherCount;
    private Text text;

    void Start()
    {
        text = GetComponent<Text>();
        try
        {
            UpdateFeatherCount();
        }
        catch
        {
            text.text = "";
        }
    }

    void Update()
    {
        UpdateFeatherCount();
    }

    void UpdateFeatherCount()
    {
        var totalFeathers = GameSettings.FeathersToWin;
        var feathersOfKokonut = kokonut.GetNumFeathers();
        text.text = string.Format("{0}/{1}", feathersOfKokonut, totalFeathers);
        VRFeatherCount.text = string.Format("{0}/{1}", feathersOfKokonut, totalFeathers);
    }
}