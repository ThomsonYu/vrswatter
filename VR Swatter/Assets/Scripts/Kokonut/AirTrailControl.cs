﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AirTrailControl : MonoBehaviour {
    private Vector3 velocity;
    private Rigidbody rb;
    private TrailRenderer trail;
    private Boolean trailOn;
    //private Vector3 minVelocity;
    // Use this for initialization
    void Start () {
        rb = GetComponentInParent<Transform>().GetComponentInParent<Rigidbody>();
        trail = GetComponent<TrailRenderer>();
        trailOn = false;


    }
	
	// Update is called once per frame
	void Update () {
        velocity = rb.velocity;
       
        if (velocity.magnitude > 8f){
            trailOn = true;
            renderAirTrail();
        } else {
            trailOn = false;
            TurnOffAirTrail();
        }
        //if (velocity.magnitude < 10f) {
        //    TurnOffAirTrail();
        //}

    }

    void renderAirTrail() {
        if (trailOn) {
            //print(trail);
            trail.enabled = true;
        }
    }

    void TurnOffAirTrail()
    {
        if (!trailOn)
        {
            trail.enabled = false;
        }
        //trail.Clear();
    }
}
