﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRTextRotation : MonoBehaviour {

    public new Camera camera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (camera == null)
        {
            camera = GameObject.Find("Kokoking/SteamVRObjects/VRCamera").GetComponent<Camera>();
        }
        Vector3 v = camera.transform.position - transform.position;
        v.x = v.z = 0.0f;
        transform.LookAt(camera.transform.position - v);
        transform.Rotate(0, 180, 0);
	}
}
