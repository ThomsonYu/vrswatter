﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class Motion : MonoBehaviour
{
    public Animator KokonutAnimator;
    private Animator PlayerAnimator;
    public Transform cameraTransform;

    public Slider jumpSlider;
    public AudioSource blowIn;
    public AudioSource blowOut;

    public float moveSpeed = 0.1f;
    public float startingcharge = 0.75f;
    public float minCharge = 2f;
    private float chargeSliderValue = 0f;
    public float maxCharge = 30f;
    public float chargetime = 1;
    public float terminalvelocity = -2;

    public float maxVelocity = 30f;

    private Rigidbody rb;
    private Vector3 move;

    private Kokonut kokonut;
    private string playerName = "";

    private GameObject SuckAir;

    private bool onGround = false;
    private int _jumpCount = 0;
    private int _diveCount = 0;
    public int JumpCount
    {
        get { return _jumpCount; }
    }
    public int DiveCount
    {
        get { return _diveCount; }
    }

    void Start()
    {
        SuckAir = transform.Find("KOKO/SuckAir").gameObject;
        rb = GetComponent<Rigidbody>();
        kokonut = GetComponent<Kokonut>();
        rb.mass = 0.5f;
        foreach (KeyValuePair<int, string> entry in GameSettings.ControllerSet)
        {
            if (entry.Value == kokonut.GetID())
            {
                playerName = "P" + entry.Key.ToString();
            }
        }
        PlayerAnimator = GetComponent<Animator>();
        if (jumpSlider)
        {
            jumpSlider.maxValue = maxCharge;
            jumpSlider.value = 0;
        }
    }

    private string PlayerControl(string control)
    {
        if (playerName == "")
        {
            playerName = "P0";
        }
        return playerName + "_" + control;
    }

    //charge jump
    void Update()
    {
        playerName = kokonut.GetControllerID();
        if (Input.GetButtonDown(PlayerControl("Jump")))
        {
            SuckAir.SetActive(true);
            TurnAnimOff();
            KokonutAnimator.SetTrigger("SuckAir");
            onGround = false;
            KokonutAnimator.SetTrigger("Idle");
            blowIn.Play();
        }

        if (Input.GetButton(PlayerControl("Jump")))
        {
            chargetime += minCharge * Time.deltaTime;
            chargeSliderValue += Time.deltaTime * 1.5f;
            UpdateJumpSlider(chargeSliderValue);
            if (chargetime * 2.1 > maxCharge)
            {
                SuckAir.SetActive(false);
            }
        }
        if (Input.GetButtonUp(PlayerControl("Jump")))
        {
            _jumpCount++;
            SuckAir.SetActive(false);
            KokonutAnimator.SetTrigger("BlowOut");
            KokonutAnimator.SetTrigger("Idle");
            chargetime = chargetime * chargetime * chargetime;
            chargeSliderValue = 0f;
            UpdateJumpSlider(chargeSliderValue);
            Jump();
            blowIn.Stop();
            blowOut.PlayDelayed(0.05f);
            chargetime = startingcharge;
            if (KokonutAnimator.GetCurrentAnimatorStateInfo(0).IsName("BlowOut"))
            {
                KokonutAnimator.SetTrigger("Idle");
            }
        }

        if (rb.velocity.y != 0)
        {
            if (Input.GetButtonDown(PlayerControl("Drop")))
            {
                TurnAnimOff();
                KokonutAnimator.SetTrigger("Dive");
                KokonutAnimator.SetTrigger("DiveIdle");
            }
        }

        if (onGround && Input.GetButton(PlayerControl("Drop")))
        {
            TurnAnimOff();
            KokonutAnimator.SetTrigger("DiveStop");
            KokonutAnimator.SetTrigger("Idle");
        }

        if (Input.GetButton(PlayerControl("Drop")))
        {
            rb.AddForce(Physics.gravity * rb.mass * 4.0f);
        }

        if (Input.GetButtonUp(PlayerControl("Drop")))
        {
            _diveCount++;
            KokonutAnimator.SetTrigger("DiveStop");
            KokonutAnimator.SetTrigger("Idle");
        }

        if (Input.GetButtonUp(PlayerControl("Drop")) && rb.velocity.y < terminalvelocity)
        {
            rb.velocity = new Vector3(rb.velocity.x, terminalvelocity, rb.velocity.z);
        }

        // On ground walking animations
        if (onGround)
        {
            if (rb.velocity.magnitude <= 1)
            {
                TurnAnimOff();
                KokonutAnimator.SetTrigger("OnGroundIdle");
            }
            else
            {
                TurnAnimOff();
                KokonutAnimator.SetTrigger("OnGroundWalk");
            }
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        var playerTransform = rb.transform;

        //terminal velocity
        if (rb.velocity.y > terminalvelocity)
        {
            rb.AddForce(Physics.gravity * rb.mass);
        }

        /*
        if ((! Input.GetButton("Drop")) && rb.velocity.y < terminalvelocity)
        {
            rb.velocity = new Vector3 (rb.velocity.x, terminalvelocity, rb.velocity.z);
        }
        */

        rb.MoveRotation(cameraTransform.transform.rotation);

        float h = Input.GetAxisRaw(PlayerControl("Horizontal"));
        float v = Input.GetAxisRaw(PlayerControl("Vertical"));

        if (h < 0.4 && h > -0.4)
        {
            PlayerAnimator.SetBool("LeanLeft", false);
            PlayerAnimator.SetBool("LeanRight", false);
            PlayerAnimator.SetBool("NoLean", true);
        }
        else
        {
            PlayerAnimator.SetBool("NoLean", false);
            if (h < -0.4)
            {
                PlayerAnimator.SetBool("LeanRight", false);
                PlayerAnimator.SetBool("LeanLeft", true);
            }
            else if (h > 0.4)
            {
                PlayerAnimator.SetBool("LeanLeft", false);
                PlayerAnimator.SetBool("LeanRight", true);
            }
        }

        move = new Vector3(h, 0, v);
        move = rb.transform.TransformDirection(move);
        if (move.y > 0)
        {
            move.y = 0f;
        }
        rb.velocity = new Vector3(move.x * moveSpeed, rb.velocity.y, move.z * moveSpeed);

        //rb.AddForce(move * moveSpeed, ForceMode.Impulse);
        //rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVelocity);
    }

    private void Jump()
    {
        chargetime = Math.Min(chargetime, maxCharge);  //max charge time
        if (rb.velocity.y < 0)
        {                       // if falling, slow fall before adding force
            rb.velocity = new Vector3(rb.velocity.x, -1f, rb.velocity.z);
        }
        rb.AddForce(new Vector3(0, 1f, 0) * chargetime, ForceMode.Impulse);
        chargetime = startingcharge;
    }

    private void UpdateJumpSlider(float charge)
    {
        if (jumpSlider)
        {
            jumpSlider.value = charge * minCharge * 2;
        }
        else
        {
            Debug.LogWarning("Slider is not initialized for " + playerName);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            onGround = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            onGround = false;
            KokonutAnimator.SetTrigger("Idle");
        }
    }

    private void TurnAnimOff()
    {
        kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("TurnLeft", false);
        kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("TurnRight", false);
        kokonut.gameObject.transform.Find("KOKO").GetComponent<Animator>().SetBool("NoTurn", true);
    }
}
