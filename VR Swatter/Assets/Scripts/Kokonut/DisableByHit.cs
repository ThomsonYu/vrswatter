﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableByHit : MonoBehaviour {

    private Kokonut kokonut;
    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        kokonut = GetComponent<Kokonut>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor" && kokonut.IsSmacked())
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }
}
