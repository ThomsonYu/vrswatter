﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SettingsCanvasManager : MonoBehaviour
{
    public delegate void exitMenuClicked();
    public event exitMenuClicked OnSettingsExit;

    public GameObject SecondsInRound;
    public GameObject FeathersToWin;
    public GameObject FeathersInRound;

    public GameObject ShowHelpers;
    public Button SaveButton;
    public Button ResetButton;
    public Button QuitButton;
    public Color ActiveSettingColor;
    public Color InactiveSettingColor;


    enum EColor { Active, Inactive }
    enum ActiveSettingChoice { FeathersToWin = 0, FeathersInRound = 1, SecondsInRound = 2, ShowHelpers = 4, Save = 5, ResetSettings = 6, Quit = 7 }
    private ActiveSettingChoice currentSetting = ActiveSettingChoice.FeathersInRound;

    private bool xAxisInUse = false;
    private bool yAxisInUse = false;


    void Start()
    {
        ResetCanvas();
        UpdateText();
    }

    void Update()
    {
        UpdateCurrentSettingState();
        UpdateColor();
        UpdateCurrentSetting();
        UpdateText();
    }

    private bool UpdateCurrentSettingState()
    {
        var dpadY = Input.GetAxisRaw("DpadY");
        var pressed = AxisAsButton(dpadY, ref yAxisInUse);
        if (!pressed) return false;
        var up = dpadY == -1;
        currentSetting = up ? currentSetting.Next() : currentSetting.Prev();
        return true;
    }

    private void UpdateCurrentSetting()
    {
        var dpadX = Input.GetAxisRaw("DpadX");
        var pressed = AxisAsButton(dpadX, ref xAxisInUse);
        var increase = dpadX == 1;
        switch (currentSetting)
        {
            case ActiveSettingChoice.SecondsInRound:
                {
                    if (!pressed) return;
                    var isMax = GameSettings.SecondsInRound == GameSettings.MaxSecondsInRound;
                    var isMin = GameSettings.SecondsInRound == GameSettings.MinSecondsInRound;
                    var carouselValue = (!increase && isMin) || (increase && isMax);
                    if (carouselValue)
                    {
                        GameSettings.SecondsInRound = increase ? GameSettings.MinSecondsInRound : GameSettings.MaxSecondsInRound;
                        return;
                    }
                    else
                    {
                        if (increase)
                            GameSettings.IncreaseSecondsInRound();
                        else
                            GameSettings.DecreaseSecondsInRound();
                    }

                    break;
                }
            case ActiveSettingChoice.FeathersToWin:
                {
                    if (!pressed) return;
                    if (increase)
                        GameSettings.IncreaseFeathersToWin();
                    else
                        GameSettings.DecreaseFeathersToWin();
                    break;
                }
            case ActiveSettingChoice.FeathersInRound:
                {
                    if (!pressed) return;
                    if (increase)
                        GameSettings.IncreaseFeathersInRound();
                    else
                        GameSettings.DecreaseFeathersInRound();

                    break;
                }
            case ActiveSettingChoice.ShowHelpers:
                {
                    if (!pressed) return;
                    GameSettings.ToggleHelpers();
                    break;
                }
            case ActiveSettingChoice.Save:
                {
                    var buttonPressed = false;
                    for (int i = 0; i < 5; i++)
                        buttonPressed |= Input.GetButtonDown("P" + i + "_X");
                    if (buttonPressed && OnSettingsExit != null) OnSettingsExit();
                    break;
                }
            case ActiveSettingChoice.ResetSettings:
                {
                    var buttonPressed = false;
                    for (int i = 0; i < 5; i++)
                        buttonPressed |= Input.GetButtonDown("P" + i + "_X");
                    if (buttonPressed)
                        GameSettings.ResetSettings();
                    break;
                }
            case ActiveSettingChoice.Quit:
                {
                    var buttonPressed = false;
                    for (int i = 0; i < 5; i++)
                        buttonPressed |= Input.GetButtonDown("P" + i + "_X");
                    if (buttonPressed)
                        Application.Quit();
                    break;
                }
        }
    }
    public void ResetCanvas()
    {
        currentSetting = ActiveSettingChoice.FeathersInRound;
        UpdateColor();
        xAxisInUse = false;
        yAxisInUse = false;
    }

    private bool AxisAsButton(float axisRaw, ref bool axisLock)
    {
        var pressed = axisRaw != 0;
        var axisReset = axisRaw == 0;
        if (axisLock && axisReset)
        {
            axisLock = false;
            return false;
        }

        if (axisLock && pressed)
        {
            // axis is in use
            return false;
        }

        if (!pressed) return false;
        axisLock = true;
        return true;
    }


    private void UpdateColor()
    {
        switch (currentSetting)
        {
            case ActiveSettingChoice.ResetSettings:
                UpdateColors(resetButton: EColor.Active);
                break;
            case ActiveSettingChoice.SecondsInRound:
                UpdateColors(secondsInRound: EColor.Active);
                break;
            case ActiveSettingChoice.FeathersToWin:
                UpdateColors(feathersToWin: EColor.Active);
                break;
            case ActiveSettingChoice.FeathersInRound:
                UpdateColors(feathersInRound: EColor.Active);
                break;
            case ActiveSettingChoice.ShowHelpers:
                UpdateColors(showHelpers: EColor.Active);
                break;
            case ActiveSettingChoice.Save:
                UpdateColors(saveButton: EColor.Active);
                break;
            case ActiveSettingChoice.Quit:
                UpdateColors(quitButton: EColor.Active);
                break;
        }

    }
    private void UpdateText()
    {
        UpdateFeatherInRoundText();
        UpdateFeathersToWinText();
        UpdateSecondsInRoundText();
        UpdateShowHelpersText();
    }

    private void UpdateShowHelpersText()
    {
        Text text = ShowHelpers.transform.Find("Value").gameObject.GetComponent<Text>();
        var showHelpers = GameSettings.ShowHelpers ? "YES" : "NO";
        text.text = "◀ " + showHelpers + " ▶";
    }

    private void UpdateFeathersToWinText()
    {
        Text text = FeathersToWin.transform.Find("Value").gameObject.GetComponent<Text>();
        text.text = "◀ " + GameSettings.FeathersToWin.ToString() + " ▶";
    }
    private void UpdateFeatherInRoundText()
    {
        Text text = FeathersInRound.transform.Find("Value").gameObject.GetComponent<Text>();
        text.text = "◀ " + GameSettings.FeathersInRound.ToString() + " ▶";
    }
    private void UpdateSecondsInRoundText()
    {
        Text text = SecondsInRound.transform.Find("Value").gameObject.GetComponent<Text>();

        TimeSpan time = TimeSpan.FromSeconds(GameSettings.SecondsInRound);
        string timeInRound = string.Format("{0:D1}:{1:D2}", time.Minutes, time.Seconds);
        text.text = "◀ " + timeInRound + " ▶";
    }

    private void UpdateSettingsColor(GameObject setting, Color color)
    {
        Text text = setting.transform.Find("Value").gameObject.GetComponent<Text>();
        Text label = setting.transform.Find("Label").gameObject.GetComponent<Text>();

        text.color = color;
        label.color = color;
    }
    private void UpdateColors(
        EColor secondsInRound = EColor.Inactive,
        EColor feathersToWin = EColor.Inactive,
        EColor feathersInRound = EColor.Inactive,
        EColor showHelpers = EColor.Inactive,
        EColor saveButton = EColor.Inactive,
        EColor quitButton = EColor.Inactive,
        EColor resetButton = EColor.Inactive
        )
    {
        UpdateSettingsColor(SecondsInRound, GetColor(secondsInRound));
        UpdateSettingsColor(FeathersToWin, GetColor(feathersToWin));
        UpdateSettingsColor(FeathersInRound, GetColor(feathersInRound));
        UpdateSettingsColor(ShowHelpers, GetColor(showHelpers));
        ResetButton.GetComponent<Image>().color = GetColor(resetButton);
        SaveButton.GetComponent<Image>().color = GetColor(saveButton);
        QuitButton.GetComponent<Image>().color = GetColor(quitButton);
    }

    private Color GetColor(EColor color)
    {
        return color == EColor.Active ? ActiveSettingColor : InactiveSettingColor;
    }

}