﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{

    public static int numPlayers;
    public static int FeathersInRound = 10;
    public static int FeathersToWin = 20;
    public static int SecondsInRound = 120;
    public static bool ShowHelpers = true;

    public static Dictionary<string, bool> Invert = new Dictionary<string, bool>();

    public static Dictionary<int, string> ControllerSet = new Dictionary<int, string>();
    public static int MaxSecondsInRound = 300;
    public static int MinSecondsInRound = 30;

    public readonly static int MaxFeathersInRound = 30;
    public readonly static int MinFeathersInRound = 1;
    public readonly static int MaxFeathersToWin = 50;
    public readonly static int MinFeathersToWin = 5;
    public readonly static int DefaultFeathersInRound = 10;
    public readonly static int DefaultFeathersToWin = 20;
    public readonly static int DefaultSecondsInRound = 120;

    public static List<HighScores> _highScores = new List<HighScores>();

    public static bool VRVibration = true;

    void Awake()
    {
        Invert.Clear();
        ControllerSet.Clear();
        Invert.Add("P0", false);
        Invert.Add("P1", false);
        Invert.Add("P2", false);
        Invert.Add("P3", false);
        Invert.Add("P4", false);

        Load();
        DontDestroyOnLoad(this);
    }
    public static void IncreaseSecondsInRound()
    {
        SecondsInRound += 10;
        if (SecondsInRound > MaxSecondsInRound)
        {
            SecondsInRound = MaxSecondsInRound;
        }
    }

    public static void DecreaseSecondsInRound()
    {
        SecondsInRound -= 10;
        if (SecondsInRound < MinSecondsInRound)
        {
            SecondsInRound = MinSecondsInRound;
        }
    }

    public static void DecreaseFeathersInRound()
    {
        FeathersInRound -= 1;
        if (FeathersInRound < MinFeathersInRound)
        {
            FeathersInRound = MinFeathersInRound;
        }
    }

    public static void IncreaseFeathersInRound()
    {
        FeathersInRound += 1;
        if (FeathersInRound > MaxFeathersInRound)
        {
            FeathersInRound = MaxFeathersInRound;
        }
    }
    public static void DecreaseFeathersToWin()
    {
        if (FeathersToWin == 5) FeathersToWin = 1;
        else FeathersToWin -= 5;
        if (FeathersToWin < MinFeathersToWin)
        {
            FeathersToWin = MinFeathersToWin;
        }
    }

    public static void IncreaseFeathersToWin()
    {
        if (FeathersToWin == 1) FeathersToWin = 5;
        else FeathersToWin += 5;
        if (FeathersToWin > MaxFeathersToWin)
        {
            FeathersToWin = MaxFeathersToWin;
        }
    }

    public static void ResetSettings()
    {
        FeathersInRound = DefaultFeathersInRound;
        FeathersToWin = DefaultFeathersToWin;
        SecondsInRound = DefaultSecondsInRound;
    }

    public static void UpdateLeaderboard(string name, int score)
    {
        HighScores hScore = new HighScores()
        {
            Name = name,
            Score = score
        };

        _highScores.Add(hScore);
        SortScores();
    }

    public static void ToggleHelpers()
    {
        ShowHelpers = !ShowHelpers;
    }

    private static void SortScores()
    {
        if (_highScores.Count == 0)
            return;

        _highScores.Sort(delegate (HighScores x, HighScores y)
        {
            return x.Score.CompareTo(y.Score);
        });

        _highScores.Reverse();
    }

    public static void UpdateLeaderboardDisplay(GameObject leaderboard)
    {
        int j = 1;
        foreach (HighScores h in _highScores)
        {
            if (j == 11)
            {
                return;
            }

            leaderboard.transform.Find("Leaderboard" + j.ToString()).gameObject.GetComponent<Text>().text = j.ToString() + ". " + h.Name;
            leaderboard.transform.Find("Leaderboard" + j.ToString() + "/Score").gameObject.GetComponent<Text>().text = h.Score.ToString();
            j++;
        }
        if (_highScores.Count == 0)
        {
            for (int i = 1; i < 11; i++)
            {
                leaderboard.transform.Find("Leaderboard" + i.ToString()).gameObject.GetComponent<Text>().text = i.ToString() + ".";
                leaderboard.transform.Find("Leaderboard" + i.ToString() + "/Score").gameObject.GetComponent<Text>().text = "";
            }
        }
    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/HighScores.bin"))
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/HighScores.bin", FileMode.Open);
                SavedScores hs = (SavedScores)bf.Deserialize(file);
                file.Close();
                _highScores = hs.highScores;
            }
            catch
            {
                // Ignore
            }
        }
    }

    public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/HighScores.bin");

        SavedScores hs = new SavedScores()
        {
            highScores = _highScores
        };

        bf.Serialize(file, hs);
        file.Close();
    }

    public static void ClearLeaderboard()
    {
        if (File.Exists(Application.persistentDataPath + "/HighScores.bin"))
        {
            _highScores.Clear();
            File.Delete(Application.persistentDataPath + "/HighScores.bin");
        }
    }
}

[System.Serializable]
public class SavedScores
{
    public List<HighScores> highScores = new List<HighScores>();
}

[System.Serializable]
public class HighScores
{
    public string Name;
    public int Score;
}