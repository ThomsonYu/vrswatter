﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    public AudioSource Music;
    private float volume = 0;

	// Use this for initialization
	void Start () {
        Music.volume = 0;
	}
	
	// Update is called once per frame
	void Update () {
        fadeIn();
	}

    private void fadeIn()
    {
        if (Music.volume < 1f)
        {
            volume += 0.2f * Time.deltaTime;
            Music.volume = volume;
        }
    }

    public void fadeOut()
    {
        if (Music.volume > 0f)
        {
            volume -= 0.5f * Time.deltaTime;
            Music.volume = volume;
        }
    }
}
