﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockSpawner : MonoBehaviour {

    public GameObject[] Rocks;
    public Transform SpawnPoint;

    private bool Spawn = false;
    private float SpawnTime = 0f;
    private float SpawnDelay = 0.5f;

	// Use this for initialization
	void Start () {
        GameObject Rock = Rocks[Random.Range(0, Rocks.Length)];
        Instantiate(Rock, SpawnPoint.position, SpawnPoint.rotation);
    }
	
	// Update is called once per frame
	void Update () {
		if (Spawn)
        {
            SpawnTime += Time.deltaTime;
            if (SpawnTime >= SpawnDelay)
            {
                SpawnRock();
                SpawnTime = 0f;
            }
        }
	}

    void SpawnRock()
    {
        GameObject Rock = Rocks[Random.Range(0, Rocks.Length)];
        Instantiate(Rock, SpawnPoint.position, SpawnPoint.rotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Throwable")
        {
            Spawn = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Throwable")
        {
            Spawn = true;
        }
    }
}
