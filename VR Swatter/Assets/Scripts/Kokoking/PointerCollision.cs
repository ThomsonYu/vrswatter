﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointerCollision : MonoBehaviour {

    public string character;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "KeyButton")
        {
            character = other.gameObject.GetComponentInChildren<Text>().text;
        }

    }
}
