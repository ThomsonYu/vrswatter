﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class FireBallSpawner : MonoBehaviour {

    public AudioClip[] fireballClips;
    public AudioClip[] fireballBarrageClips;

    public GameObject Fireball;
    public Transform Spawnpoint;
    public float speed;
    public FireBallSpawner otherHand;
    private Hand thisHand;

    public Slider SpamSlider;
    public GameObject SpamReadyText;
    public Image SliderBackground;
    public Animator VRUIAnimator;
    public Animator HandAnimator;
    private GameObject HandModel;

    private bool vibrate = true;
    private float vibrateTimer = 0f;
    private float vibrateDelay = 2f;

    public Slider ShootSlider;
    private bool Shooting = false;
    private float fireTimer = 0f;
    private float fireDelay = 0.9f;

    private float firstShotTimer = 0f;
    private float firstShotDelay = 0.9f;
	private bool firstFired = false;

    private bool spamEnabled = false;
    private float spamCoolDownTimer = 0f;
    private float spamCoolDownDelay = 15f;
    private float spamTime = 6f;

    [SteamVR_DefaultAction("Interact")]
    public SteamVR_Action_Boolean spawn;

    [SteamVR_DefaultAction("Interact")]
    public SteamVR_Action_Boolean gripReady;

    [SteamVR_DefaultAction("Interact")]
    public SteamVR_Action_Boolean vibrationToggle;

    SteamVR_Behaviour_Pose trackedObj;

    private Color gray = new Color(0.5f, 0.5f, 0.5f, 0.5f);
    private Color red = new Color(1, 0.5f, 0, 0.75f);

    // Use this for initialization
    void Start () {
        trackedObj = GetComponent<SteamVR_Behaviour_Pose>();
        thisHand = GetComponent<Hand>();
        if (ShootSlider)
        {
            ShootSlider.maxValue = fireDelay;
            ShootSlider.value = 0;
        }

        if (SpamSlider)
        {
            SpamSlider.maxValue = spamCoolDownDelay;
            SpamSlider.value = 0;
        }
        HandModel = HandAnimator.transform.Find("Hand_Model").gameObject;
        HandModel.GetComponent<Animator>().SetBool("UnSpam", true);
    }

    private void Update()
    {
        if (vibrationToggle.GetStateDown(trackedObj.inputSource))
        {
            if (GameSettings.VRVibration)
            {
                GameSettings.VRVibration = false;
            }
            else
            {
                GameSettings.VRVibration = true;
            }
        }


        if (gripReady.GetStateDown(trackedObj.inputSource))
        {
            HandAnimator.SetBool("Idle", false);
            HandAnimator.SetBool("GrabEnd", false);
            HandAnimator.SetBool("Grab", true);
            HandAnimator.SetBool("GrabIdle", true);
        }
        if (gripReady.GetStateUp(trackedObj.inputSource))
        {
            HandAnimator.SetBool("Grab", false);
            HandAnimator.SetBool("GrabIdle", false);
            HandAnimator.SetBool("GrabEnd", true);
            HandAnimator.SetBool("Idle", true);
            HandAnimator.SetBool("GrabEnd", false);
        }

        if (!spamEnabled)
        {
            PrimaryShoot();
            spamCoolDownTimer += Time.deltaTime;
            SpamSlider.value = spamCoolDownTimer;
            SliderBackground.color = Color.Lerp(gray, red, SpamSlider.value / SpamSlider.maxValue);
            SliderBackground.color.ColorWithAlpha(100);
            if (spamCoolDownTimer >= spamCoolDownDelay)
            {
                SpamReadyText.SetActive(true);
                VRUIAnimator.SetTrigger("SpamReady");
                if (GameSettings.VRVibration)
                {
                    if (vibrate)
                    {
                        for (float i = 0; i < 0.5f; i += Time.deltaTime)
                        {
                            thisHand.TriggerHapticPulse(10000);
                        }
                        vibrate = false;
                        vibrateTimer = 0f;
                    }
                    if (!vibrate)
                    {
                        vibrateTimer += Time.deltaTime;
                        if (vibrateTimer >= vibrateDelay)
                        {
                            vibrate = true;
                        }
                    }
                }
                HandModel.GetComponent<Animator>().SetBool("SpamReady",true);
                HandModel.GetComponent<Animator>().SetBool("UnSpam", false);
                spamCoolDownTimer = spamCoolDownDelay;
                if (gripReady.GetState(trackedObj.inputSource) && otherHand.gripReady.GetState(otherHand.GetComponent<SteamVR_Behaviour_Pose>().inputSource)
                    && spawn.GetState(trackedObj.inputSource) && otherHand.spawn.GetState(otherHand.GetComponent<SteamVR_Behaviour_Pose>().inputSource))
                {
                    HandModel.GetComponent<Animator>().SetBool("Spam", true);
                    HandModel.GetComponent<Animator>().SetBool("SpamReady", false);
                    spamEnabled = true;
                }
            }
        }
        else
        {
            fireDelay = 0.1f;
            firstShotDelay = 0.1f;
            PrimaryShoot();

            spamCoolDownTimer -= Time.deltaTime * spamTime;
            SpamSlider.value = spamCoolDownTimer;
            SliderBackground.color = Color.Lerp(red, gray, SpamSlider.value / SpamSlider.maxValue);
            SliderBackground.color.ColorWithAlpha(5000);
            if (spamCoolDownTimer <= 0f)
            {
                spamEnabled = false;
                vibrate = true;
                HandModel.GetComponent<Animator>().SetBool("UnSpam", true);
                HandModel.GetComponent<Animator>().SetBool("SpamReady", false);
                HandModel.GetComponent<Animator>().SetBool("Spam", false);
                SpamReadyText.SetActive(false);
                fireDelay = 0.9f;
                firstShotDelay = 0.9f;
                spamCoolDownTimer = 0f;
            }
        }
    }

    void PrimaryShoot()
    {
        if (firstFired)
        {
            firstShotTimer += Time.deltaTime;
            ShootSlider.value = firstShotTimer;
        }

        if (firstShotTimer >= firstShotDelay)
        {
            firstFired = false;
            firstShotTimer = 0f;
        }

        if (spawn.GetStateDown(trackedObj.inputSource))
        {
            Shooting = true;
            HandAnimator.SetBool("Idle", false);
            HandAnimator.SetBool("ShootEnd", false);
            HandAnimator.SetBool("Shoot", true);
            HandAnimator.SetBool("ShootIdle", true);
            if (!firstFired)
            {
                Shoot();
                firstFired = true;
                firstShotTimer = 0f;
            }
        }
        if (spawn.GetStateUp(trackedObj.inputSource))
        {
            HandAnimator.SetBool("Shoot", false);
            HandAnimator.SetBool("ShootIdle", false);
            HandAnimator.SetBool("ShootEnd", true);
            HandAnimator.SetBool("Idle", true);
            HandAnimator.SetBool("ShootEnd", false);
            Shooting = false;
            fireTimer = 0f;
        }

        if (Shooting)
        {
            if (fireTimer < fireDelay)
            {
                fireTimer += Time.deltaTime;
            }
            if (fireTimer >= fireDelay)
            {
                Shoot();
                fireTimer = 0f;
                firstShotTimer = 0f;
            }
        }
    }

    void Shoot()
    {
        GameObject fireball = Instantiate(Fireball, Spawnpoint.position, Spawnpoint.rotation);
        if (spamEnabled)
        {
            fireball.GetComponent<FireBallPhysics>().SetScore(100);
            fireball.GetComponent<AudioSource>().clip = fireballBarrageClips[Random.Range(0, fireballBarrageClips.Length)];
            fireball.GetComponent<AudioSource>().volume = 0.15f;
        }
        else
        {
            fireball.GetComponent<FireBallPhysics>().SetScore(100);
            fireball.GetComponent<AudioSource>().clip = fireballClips[Random.Range(0, fireballClips.Length)];
            fireball.GetComponent<AudioSource>().volume = 0.75f;
        }
        Rigidbody rb = fireball.GetComponent<Rigidbody>();
        rb.velocity = Spawnpoint.forward * speed;
        fireball.GetComponent<AudioSource>().Play();
    }

    /*
    public void AnimateHandWithoutController()
    {
        for (int handIndex = 0; handIndex < Player.instance.hands.Length; handIndex++)
        {
            Hand hand = Player.instance.hands[handIndex];
            if (hand != null)
            {
                hand.SetSkeletonRangeOfMotion(Valve.VR.EVRSkeletalMotionRange.WithoutController);
            }
        }
    }
    */
}
