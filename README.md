# Kokos VR Game

Kokos

A fast paced asymmetrical VR party game in which players compete for the VR throne by collecting feathers while also avoiding hazards.
## Concept 

Kokos is a 3-5 player asymmetrical VR party game! Each player takes
control of a Kokonut and collects feathers around the map by blowing air into their
leaves while the VR player, the Koko King, tries to prevent the Kokonuts from doing so
by throwing rocks and shooting fireballs at the Kokonuts. The first Kokonut to collect 
enough feathers before the round ends becomes the next Koko King! Who will be the ultimate VR God??

A game project made by students from the University of Toronto and Centennial College:    
  
Thomson Yu  
Brandon Cormier  
Chengjie (Ryan) Liang  
Lanbo (Jianxin Pei)  
Maria-Paula Bonilla Hernandez  
Hoang Luong Dam (Eric)  
Flavio Felipe  
Vinicius da Silveira  
Fari Strachan  